# Common Docker commands and Explanation

## Build Docker image

#### *Build a single docker image with path*

```sh
docker build -t helloworlddocker docker_build/
```

If you are inside `docker_build` directory, you can run following command.

```sh
docker build -t helloworlddocker .
```
#### *Build a docker image from Text file*
```sh
docker build -< Dockerfilename -t helloworlddocker
```

#### Tips & Tricks
- If you want to add any file from local file system to docker container using ADD command inside the dockerfile
- Build with path is better option, docker will look for the file in the provided path.
- In case of text file option, docker will look for another directy in the file system.

## Find built docker image

#### *Find your already built images*

```sh
docker images
```

## Delete built docker image

#### *Delete any of the image*

```sh
docker rmi $name_of_any_image
```

#### *Delete any of the image forcefully*

```sh
docker rmi $name_of_any_image -f
```


#### *Delete all unused images*

```sh
docker system prune
```

## Run a docker image

#### *Run a built image*

```sh
docker run $name_of_any_imag
```
#### *Run a built image in detached mode*

```sh
docker run -d $name_of_any_image
```
This will detach docker container stdout from your console. Container will run as a daemon.

#### Tips & Tricks

- Your docker container will stop running after executing all CMD commands inside the docker file. 
- If you want to keep it running idle until you kill it, then follow any of the below two commands. 

#### *Run a built image in interactive mode*

```sh
docker run -it $name_of_any_image
```
#### *Run a built image in detached interactive mode*

```sh
docker run -d -it $name_of_any_image
```

#### *Run and map docker container port with local machine port*

```sh
docker run -p 127.0.0.1:$hostport:$containerport $name_of_any_image
```

## Next
[How to write a Dockerfile](https://bitbucket.org/shifuddin/docker-sheet/src/Dockerfile/?at=master)